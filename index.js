const Joi = require('joi');
const express = require('express');
const app = express();

app.use(express.json());

const tasks = [
    {id: 1, name : 'alfin'},
    {id: 2, name : 'rizki'},
    {id: 3, name : 'rina'},
]

app.get('/', (req, res) => {
   
});

app.get('/api/tasks', (req, res) => {
    res.send(tasks);
});

app.get('/api/tasks/:id', (req, res) => {
    const task = tasks.find(c => c.id === parseInt(req.params.id));
    if (!task) return res.status(404).send('not found');
    res.send(task); 
});

app.post('/api/tasks', (req, res)=>{
    const {error} = validateTask(req.body);
    if (error) return res.status(400).send(result.error.details[0].message);
    

    const task ={
        id: tasks.length +1,
        name: req.body.name
    };
    tasks.push(task);
    res.send(task);
});

app.put('/api/tasks/:id', (req, res)=>{
    const task = tasks.find(c => c.id === parseInt(req.params.id));
    if (!task) return res.status(404).send('not found');
        
    

    const {error} = validateTask(req.body);
    if (error) return res.status(400).send(result.error.details[0].message);
        
    

    task.name = req.body.name;
    res.send(task);

});

function validateTask(task){
    const schema = {
        name: Joi.string().min(3).required()
    };
    return Joi.validate(task, schema);
}

app.delete('/api/tasks/:id', (req, res)=>{
    const task = tasks.find(c => c.id === parseInt(req.params.id));
    if (!task) return res.status(404).send('not found');

    const index = tasks.indexOf(task);
    tasks.splice(index, 1);

    res.send(task);

});

const port = process.env.PORT || 3000
app.listen(port, ()=>console.log(`Listening on port ${port}...`));
